import { combineReducers } from 'redux';

import deck from '../Components/Deck/Deck.reducer';
import player from '../Components/Player/Player.reducer';

const reducers = combineReducers({
  deck,
  player,
});

export default reducers;
