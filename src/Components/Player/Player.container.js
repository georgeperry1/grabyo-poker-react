import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Player from './Player';
import { openPlayerModal, removePlayer } from './Player.actions';

class PlayerContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      flipped: false,
    };
  }

  static propTypes = {
    hands: PropTypes.array,
    openPlayerModal: PropTypes.func,
    removePlayer: PropTypes.func,
    id: PropTypes.number,
  };

  static defaultProps = {
    openPlayerModal: () => {},
    removePlayer: () => {},
  };

  UNSAFE_componentWillUpdate(nextProps) {
    const currentHands = this.props.hands;
    const updatedHands = nextProps.hands;
    // If the hands are different, the flipping functionality is reset 
    // so that the animation can happen on every new round
    if (currentHands !== updatedHands) {
      this.setState({ flipped: false });
      this.handleFlipCards();
    }
  }

  handleEditClick = id => {
    // All editing is done vie the App components
    const { openPlayerModal } = this.props;
    openPlayerModal('edit', id);
  };

  handleRemoveClick = id => {
    const { removePlayer } = this.props;
    removePlayer(id);
  };

  handleFlipCards = () => {
    // Delay for the flip animation 
    setTimeout(() => {
      this.setState({ flipped: true });
    }, 1000);
  };

  render() {
    const { hands, id } = this.props;
    const { flipped } = this.state;
    const hand = hands[id - 1];

    return (
      <Player
        hand={hand}
        flipped={flipped}
        handleEditClick={this.handleEditClick}
        handleRemoveClick={this.handleRemoveClick}
        {...this.props}
      />
    );
  }
}

const mapStateToProps = ({ deck }) => ({
  hands: deck.hands,
});

const mapDispatchToProps = dispatch => ({
  openPlayerModal: (type, id) => dispatch(openPlayerModal(type, id)),
  removePlayer: id => dispatch(removePlayer(id)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PlayerContainer);
