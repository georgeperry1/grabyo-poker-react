import {
  ADD_PLAYER,
  REMOVE_PLAYER,
  EDIT_PLAYER_NAME,
  OPEN_PLAYER_MODAL,
  CLOSE_PLAYER_MODAL,
} from './Player.actionTypes';

export const addPlayer = name => dispatch => {
  dispatch({
    type: ADD_PLAYER,
    payload: name,
  });
};

export const removePlayer = id => dispatch => {
  dispatch({
    type: REMOVE_PLAYER,
    payload: id,
  });
};

export const editPlayerName = (name, id) => dispatch => {
  dispatch({
    type: EDIT_PLAYER_NAME,
    payload: {
      id,
      name,
    },
  });
};

export const openPlayerModal = (type, id = 0) => dispatch => {
  dispatch({
    type: OPEN_PLAYER_MODAL,
    payload: {
      type,
      id,
    },
  });
};

export const closePlayerModal = () => dispatch => {
  dispatch({
    type: CLOSE_PLAYER_MODAL,
  });
};
