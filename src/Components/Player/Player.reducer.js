import {
  ADD_PLAYER,
  REMOVE_PLAYER,
  EDIT_PLAYER_NAME,
  OPEN_PLAYER_MODAL,
  CLOSE_PLAYER_MODAL,
} from './Player.actionTypes';

const defaultState = {
  players: [],
  isOpen: false,
  type: '',
  editId: 0,
};

const playerReducer = (
  state = defaultState,
  action = {
    type: '',
    payload: '',
  }
) => {
  switch (action.type) {
    case ADD_PLAYER: {
      const player = {
        name: action.payload,
        id: state.players.length + 1,
      };

      return {
        ...state,
        players: [...state.players, player],
        isOpen: true,
      };
    }

    case REMOVE_PLAYER: {
      const newPlayers = state.players.filter(
        player => player.id !== action.payload
      );
      return {
        ...state,
        players: newPlayers,
      };
    }

    case EDIT_PLAYER_NAME: {
      const removePlayerToEdit = state.players.filter(
        player => player.id !== action.payload.id
      );
      return {
        ...state,
        players: [...removePlayerToEdit, action.payload],
        isOpen: true,
      };
    }

    case OPEN_PLAYER_MODAL: {
      return {
        ...state,
        isOpen: true,
        type: action.payload.type,
        editId: action.payload.id,
      };
    }

    case CLOSE_PLAYER_MODAL: {
      return {
        ...state,
        isOpen: false,
      };
    }

    default:
      return state;
  }
};

export default playerReducer;
