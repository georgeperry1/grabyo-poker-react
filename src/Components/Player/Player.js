import React from 'react';
import PropTypes from 'prop-types';
import ReactCardFlip from 'react-card-flip';

import Button from '../Button';
import {
  Hand,
  PlayerName,
  HandCard,
  FrontCard,
  BackCard,
} from './Player.styled';

const Player = ({
  name,
  id,
  hand,
  handleEditClick,
  handleRemoveClick,
  flipped,
}) => (
  <article>
    <PlayerName>
      {name}
      <Button icon="✏️" onClick={e => handleEditClick(id)}>
        Edit
      </Button>
      <Button icon="🔥" onClick={e => handleRemoveClick(id)}>
        Remove
      </Button>
    </PlayerName>
    <Hand>
      {!!hand &&
        hand.map(card => (
          <HandCard key={card}>
            <ReactCardFlip isFlipped={flipped} flipDirection="horizontal">
              <FrontCard
                key="front"
                suit={card[1]}
                value={card[0]}
                hidden={true}
              />
              <BackCard
                key="back"
                suit={card[1]}
                value={card[0]}
                hidden={false}
              >
                {card[0]}
              </BackCard>
            </ReactCardFlip>
          </HandCard>
        ))}
    </Hand>
  </article>
);

Player.propTypes = {
  name: PropTypes.string,
  id: PropTypes.number,
  hand: PropTypes.array,
  handleEditClick: PropTypes.func,
  handleRemoveClick: PropTypes.func,
  flipped: PropTypes.bool,
};

Player.defaultProps = {
  handleEditClick: () => {},
  handleRemoveClick: () => {},
  flipped: false,
};

export default Player;
