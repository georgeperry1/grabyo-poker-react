import styled from 'styled-components';

import Card from '../Card';
import { StyledButton } from '../Button/Button.styled';

export const PlayerName = styled.p`
  font-weight: 700;

  ${StyledButton} {
    margin: 0px 0px 0px 15px;
  }
`;

export const Hand = styled.div`
  background: #888;
  padding: 10px;
  border-radius: 5px;
  min-height: 55px;
  display: flex;
  flex-direction: row;
`;

export const HandCard = styled.div``;

export const FrontCard = styled(Card)`
  margin: 0px 5px;
`;

export const BackCard = styled(Card)`
  margin: 0px 5px;
`;
