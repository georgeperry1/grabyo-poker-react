import React from 'react';
import PropTypes from 'prop-types';

import { StyledButton } from './Button.styled';

const Button = ({ icon, children, onClick, disabled }) => (
  <StyledButton onClick={onClick} disabled={disabled}>
    {icon && (
      <span role="img" alt="woman raising hand" aria-label="woman raising hand">
        {icon}
      </span>
    )}
    {children}
  </StyledButton>
);

Button.propTypes = {
  icon: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
    PropTypes.number,
  ]),
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
};

Button.defaultProps = {
  onClick: () => {},
  disabled: false,
};

export default Button;
