import styled from 'styled-components';

export const StyledButton = styled.button`
  background: transparent;
  border: 1px solid #eee;
  border-radius: 5px;
  color: #eee;
  padding: 0.5em;
  cursor: ${props => (!!props.disabled ? 'not-allowed' : 'pointer')};
  opacity: ${props => (!!props.disabled ? '0.4' : '1')};

  & + & {
    margin-left: 20px;
  }
`;
