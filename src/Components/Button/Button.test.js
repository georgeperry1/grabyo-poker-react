import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Button from './Button';

// Config adapter for React 16 and Enzyme
configure({ adapter: new Adapter() });

describe('Button Component', () => {
    // Basic render
    test('renders without crashing', () => {
      shallow(
          <Button>Test</Button>
        );
    });
  
    // Wrapper for Button without Icon
    const noIconWrapper = shallow(<Button>Test</Button>);

    // Wrapper for Button with Icon
    const iconProps = { icon: '🃏' };
    const iconWrapper = shallow(<Button {...iconProps}>Test</Button>);
  
    // No Icon
    test('renders button without a span when no icon is passed to props', () => {
        expect(noIconWrapper.find('span').length).toEqual(0);
    }); 
  
    // Icon
    test('renders button with the correct icon when one is passed to props', () => {
        expect(iconWrapper.find('span').length).toEqual(1);
        expect(iconWrapper.find('span').text()).toEqual('🃏');
    }); 

    // Render Children
    test('renders a child when passed', () => {
        expect(noIconWrapper.text()).toEqual('Test');
    }); 
  });
