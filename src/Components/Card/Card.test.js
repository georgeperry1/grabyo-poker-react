import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Card from './Card';
import { VisibleCard, HiddenCard } from './Card.styled';

// Config adapter for React 16 and Enzyme
configure({ adapter: new Adapter() });

describe('Card Component', () => {
    // Basic render
    test('renders without crashing', () => {
      shallow(
          <Card />
        );
    });
  
    // Wrapper for visible
    const visibleProps = { hidden: false };
    const visibleWrapper = shallow(<Card {...visibleProps}/>);
    // Wrapper for hidden card
    const hiddenProps = { hidden: true };
    const hiddenWrapper = shallow(<Card {...hiddenProps}/>);
  
    // Visible
    test('renders a card when passed props', () => {
        // Visible Crad must be imported in order to find it, as it is a Styled Component
        expect(visibleWrapper.find(VisibleCard).length).toEqual(1);
    }); 
  
    // Hidden Card
    test('renders a hidden card when passed hidden props', () => {
        // Hidden Crad must be imported in order to find it, as it is a Styled Component
        expect(hiddenWrapper.find(HiddenCard).length).toEqual(1);
    }); 
  });
