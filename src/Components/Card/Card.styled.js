import styled, { css } from 'styled-components';

import { getColourForSuit } from '../../utils';

export const VisibleCard = styled.div`
    width: 30px;
	height: 55px;
	line-height: 55px;
	border-radius: 3px;
	background: #fff;
	box-shadow: 0 0 0 2px #fff;
	position: relative;
	display: inline-block;
	text-align: center;
	font-weight: 700;

	border: 2px solid;
	border-color: ${props => getColourForSuit(props.suit)};
	color: ${props => getColourForSuit(props.suit)};

	&:after,
	&:before {
		content: "${props => props.suit}";
		position: absolute;
		font-size: 12px;
		line-height: 1.2;
		font-weight: 400;
	}

	&:after {
		top: 5px;
		right: 5px;
	}

	&:before {
		bottom: 5px;
		left: 5px;
	}

	${props =>
    props.selected
      ? css`
          background: ${getColourForSuit(props.suit)};
          box-shadow: 0 0 0 2px ${getColourForSuit(props.suit)};
          color: white;
          border-color: white;
        `
      : null}
`;

export const HiddenCard = styled.div`
  width: 30px;
  height: 55px;
  line-height: 105px;
  border-radius: 3px;
  background-image: url('http://cdn.backgroundhost.com/backgrounds/subtlepatterns/real_cf.png');
  box-shadow: 0 0 0 2px #fff;
  position: relative;
  display: inline-block;
  text-align: center;
  border: 2px solid black;
  font-weight: 700;
`;
