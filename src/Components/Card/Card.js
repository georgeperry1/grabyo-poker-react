import React from 'react';

import { VisibleCard, HiddenCard } from './Card.styled';

const Card = props => (
  <>{!!props.hidden ? <HiddenCard {...props} /> : <VisibleCard {...props} />}</>
);

export default Card;
