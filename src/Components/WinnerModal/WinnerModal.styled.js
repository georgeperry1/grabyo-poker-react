import styled from 'styled-components';

export const WinnerName = styled.h1`
  color: #eee;
  font-weight: 700;
`;
