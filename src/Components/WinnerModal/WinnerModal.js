import React from 'react';
import PropTypes from 'prop-types';
import ReactCardFlip from 'react-card-flip';

import { WinnerName } from './WinnerModal.styled';
import {
  StyledModal,
  customModalStyles,
  ModalBody,
  ModalButton,
  ModalButtonContainer,
} from '../PlayerModal/PlayerModal.styled';
import { 
  Hand,
  HandCard,
  FrontCard,
  BackCard,
} from '../Player/Player.styled';

const WinnerModal = ({ isOpen, finding, winner, handleGameOver, winningHand }) => (
  <StyledModal isOpen={isOpen} style={customModalStyles}>
    <ModalBody>
      <WinnerName>
        {!!finding ? 'And the Winner is... 🥁' : `${winner} 🥇`}
      </WinnerName>
      <Hand>
        {!!winningHand &&
          winningHand.map(card => (
          <HandCard key={card}>
            <ReactCardFlip isFlipped={!finding} flipDirection="horizontal">
              <FrontCard
                key="front"
                suit={card[1]}
                value={card[0]}
                hidden={true}
              />
              <BackCard
                key="back"
                suit={card[1]}
                value={card[0]}
                hidden={false}
              >
                {card[0]}
              </BackCard>
            </ReactCardFlip>
          </HandCard>
        ))}
      </Hand>
      <ModalButtonContainer>
        <ModalButton icon="🔁" onClick={handleGameOver} disabled={!!finding}>
          Play Again
        </ModalButton>
      </ModalButtonContainer>
    </ModalBody>
  </StyledModal>
);

WinnerModal.propTypes = {
  handleGameOver: PropTypes.func,
  winner: PropTypes.string,
  isOpen: PropTypes.bool,
  finding: PropTypes.bool,
  winningHand: PropTypes.array,
};

WinnerModal.defaultProps = {
  handleGameOver: () => {},
  isOpen: false,
  finding: false,
};

export default WinnerModal;
