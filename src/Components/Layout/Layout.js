import React from 'react';
import PropTypes from 'prop-types';

import { Main, Header } from './Layout.styled';

const Layout = ({ children }) => (
  <>
    <Header>
      <img
        src="/logo.svg"
        alt="Grabyo logo"
        width="50"
        style={{ verticalAlign: 'middle' }}
      />
      <h1>Grabyo Poker</h1>
    </Header>
    <Main role="main">{children}</Main>
  </>
);

Layout.propTypes = {
  children: PropTypes.element,
};

export default Layout;
