import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Layout from './Layout';

// Config adapter for React 16 and Enzyme
configure({ adapter: new Adapter() });

const TestChild = () => <p>Testing</p>

describe('Layout Component', () => {
    // Basic render
    test('renders without crashing', () => {
      shallow(
          <Layout>
              <TestChild />
          </Layout>
        );
    });
  
    const wrapper = shallow(
        <Layout>
            <TestChild />
        </Layout>
    );

    // Render Children
    test('renders a card when passed props', () => {
        expect(wrapper.find('TestChild').length).toEqual(1);
    }); 
  });
