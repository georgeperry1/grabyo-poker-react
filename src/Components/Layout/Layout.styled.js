import styled from 'styled-components';

export const Main = styled.main`
  max-width: 800px;
  margin: 0 auto;
  padding: 20px;

  .row {
    text-align: center;
  }
`;

export const Header = styled.header`
  max-width: 800px;
  padding: 20px 0;
  margin: 0 auto;
  border-bottom: 1px solid #eee;
  display: flex;
  align-items: center;

  img {
    margin-right: 20px;
  }
`;
