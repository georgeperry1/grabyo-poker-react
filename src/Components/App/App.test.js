import React from 'react';
import { shallow, mount, configure } from 'enzyme';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import Adapter from 'enzyme-adapter-react-16';

import App from './App.container';
import { OPEN_PLAYER_MODAL } from '../Player/Player.actionTypes';

// !! React 16.6 and Enzyme don't currently play nicely together due to React Memo 
// !! meaning this error is thrown: https://github.com/airbnb/enzyme/issues/1875
// This is the work around that mocks the React Memo API
jest.mock('react', () => {
  const r = jest.requireActual('react');
  return { ...r, memo: (x) => x };
});

// Config adapter for React 16 and Enzyme
configure({ adapter: new Adapter() });

// Initiate mockStore & defaultState
const mockStore = configureStore([thunk]);
const defaultState = {
  player: {
    players: [{ name: 'George', id: 1 }, { name: 'Isabelle', id: 2 }],
    isOpen: false,
    type: '',
    editId: 0,
  },
  deck: {
    hands: [['2C', '3C', '4C', '5C', '6C'], ['7D', '7S', '9C', '9S', '9D']],
  },
};

describe('App Component', () => {
  const store = mockStore({ ...defaultState });

  afterEach(() => {
    store.clearActions();
  });

  // Basic render
  test('renders without crashing', () => {
    shallow(
      <Provider store={store}>
        <App />
      </Provider>
      );
  });

  // Wrapper with both App.container and App
  const wrapper = mount(
    <Provider store={store}>
      <App />
    </Provider>
  );

  // Renders Players
  test('renders players when passed via Redux', () => {
    expect(wrapper.find('Player').length).toEqual(2);
  }); 

  // Dispatched Open Modal action
  test('clicking Add New Player dispatched Open Player Modal action', () => {
    // Initial state should display no modal
    expect(wrapper.find('PlayerModal').length).toEqual(0);
    const button = wrapper.find('Button').at(6);
    button.simulate('click');
    wrapper.update();

    // Simulating click should dispatch action to open modal
    const actions = store.getActions();
    expect(actions[0].type).toEqual(OPEN_PLAYER_MODAL);
  }); 
});
