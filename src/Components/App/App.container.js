import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import App from './App';
import {
  addPlayer,
  editPlayerName,
  openPlayerModal,
  closePlayerModal,
} from '../Player/Player.actions';
import { resetDeck } from '../Deck/Deck.actions';
import { compareHands } from '../../utils';

class AppContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newPlayerName: '',
      winner: '',
      winningHand: [],
      finding: false,
      winnerModalOpen: false,
    };
  }

  static propTypes = {
    players: PropTypes.array,
    isOpen: PropTypes.bool,
    type: PropTypes.string,
    editId: PropTypes.number,
    hands: PropTypes.array,
    addPlayer: PropTypes.func,
    editPlayerName: PropTypes.func,
    openPlayerModal: PropTypes.func,
    closePlayerModal: PropTypes.func,
    resetDeck: PropTypes.func,
  };

  static defaultProps = {
    isOpen: false,
    addPlayer: () => {},
    editPlayerName: () => {},
    openPlayerModal: () => {},
    closePlayerModal: () => {},
    resetDeck: () => {},
  };

  handleConfirmClick = e => {
    const { addPlayer, editPlayerName, type, editId } = this.props;
    const { newPlayerName } = this.state;

    // Saves building two modals or repeating unnecessary code
    if (type === 'add') addPlayer(newPlayerName);
    if (type === 'edit') editPlayerName(newPlayerName, editId);

    this.handleModalClose();
  };

  handleModalOpen = type => {
    const { openPlayerModal } = this.props;
    openPlayerModal(type);
  };

  handleModalClose = e => {
    const { closePlayerModal } = this.props;
    closePlayerModal();
  };

  handleInputChange = e => {
    this.setState({ newPlayerName: e.target.value });
  };

  handleFindWinnerClick = e => {
    this.setState({ finding: true, winnerModalOpen: true });

    const { hands, players } = this.props;

    // If there is only one player, they get the win!
    if (players.length === 1) this.setState({ winner: players[0].name });

    // Formats the hands into an array of strings
    // which the poker.judgeWinner method accepts
    const joinedHands = hands.map(hand => {
      return hand.join(' ');
    });

    // Get winning hand and player
    const winningHand = compareHands(joinedHands);
    const winningIndex = joinedHands.indexOf(winningHand[0]);
    const winner = players[winningIndex];

    // Set winning hand before the winner is revealled 
    // as the back of the card is rendered when the modal does
    this.setState({ 
      winningHand: winningHand[0].split(' '),
    });

    // Send the winning name and reveal after 1 second 
    setTimeout(
      () => this.setState({ 
        winner: winner.name, 
        finding: false 
      }),
      1000
    );
  };

  handleGameOver = e => {
    const { resetDeck } = this.props;
    this.setState({ winnerModalOpen: false });
    // Removes all previous hands
    resetDeck();
  };

  render() {
    const { finding, winner, winnerModalOpen, winningHand } = this.state;
    return (
      <App
        handleConfirmClick={this.handleConfirmClick}
        handleModalOpen={this.handleModalOpen}
        handleModalClose={this.handleModalClose}
        handleInputChange={this.handleInputChange}
        handleFindWinnerClick={this.handleFindWinnerClick}
        handleGameOver={this.handleGameOver}
        finding={finding}
        winnerModalOpen={winnerModalOpen}
        winner={winner}
        winningHand={winningHand}
        {...this.props}
      />
    );
  }
}

const mapStateToProps = ({ player, deck }) => ({
  players: player.players,
  playerModalOpen: player.isOpen,
  type: player.type,
  editId: player.editId,
  hands: deck.hands,
});

const mapDispatchToProps = dispatch => ({
  addPlayer: player => dispatch(addPlayer(player)),
  editPlayerName: (newName, id) => dispatch(editPlayerName(newName, id)),
  openPlayerModal: (type, id) => dispatch(openPlayerModal(type, id)),
  closePlayerModal: () => dispatch(closePlayerModal()),
  resetDeck: () => dispatch(resetDeck()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppContainer);
