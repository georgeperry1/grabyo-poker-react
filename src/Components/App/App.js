import React from 'react';
import PropTypes from 'prop-types';

import Layout from '../Layout';
import Deck from '../Deck';
import Player from '../Player';
import Button from '../Button';
import PlayerModal from '../PlayerModal';
import WinnerModal from '../WinnerModal';
import { Footer } from './App.styled';

const App = ({
  handleConfirmClick,
  handleModalOpen,
  handleModalClose,
  handleInputChange,
  handleFindWinnerClick,
  handleGameOver,
  playerModalOpen,
  players,
  type,
  hands,
  finding,
  winner,
  winnerModalOpen,
  winningHand,
}) => (
  <Layout>
    {playerModalOpen && (
      <PlayerModal
        isOpen={true}
        handleConfirmClick={handleConfirmClick}
        handleModalClose={handleModalClose}
        handleInputChange={handleInputChange}
        type={type}
      />
    )}
    {winnerModalOpen && (
      <WinnerModal
        isOpen={true}
        winner={winner}
		winningHand={winningHand}
        finding={finding}
        handleGameOver={handleGameOver}
      />
    )}
    <section>
      <h1>Cards Deck</h1>
      <Deck />
    </section>
    <section>
      <header>
        <h1>Players</h1>
      </header>
      <section>
        {players.map(player => (
          <Player name={player.name} id={player.id} key={player.id} />
        ))}
      </section>
      <Footer>
        <Button icon="🙋‍♀️" onClick={e => handleModalOpen('add')}>
          Add New Player
        </Button>
        <Button
          icon="🏆"
          onClick={handleFindWinnerClick}
          disabled={hands.length === 0}
        >
          Find the Winner
        </Button>
      </Footer>
    </section>
  </Layout>
);

App.propTypes = {
  handleConfirmClick: PropTypes.func,
  handleModalOpen: PropTypes.func,
  handleModalClose: PropTypes.func,
  handleInputChange: PropTypes.func,
  handleFindWinnerClick: PropTypes.func,
  handleGameOver: PropTypes.func,
  playerModalOpen: PropTypes.bool,
  players: PropTypes.array,
  type: PropTypes.string,
  hands: PropTypes.array,
  finding: PropTypes.bool,
  winner: PropTypes.string,
  winningHand: PropTypes.array,
  winnerModalOpen: PropTypes.bool,
};

App.defaultProps = {
  handleConfirmClick: () => {},
  handleModalOpen: () => {},
  handleModalClose: () => {},
  handleInputChange: () => {},
  handleFindWinnerClick: () => {},
  handleGameOver: () => {},
  playerModalOpen: false,
  winnerModalOpen: false,
};

export default App;
