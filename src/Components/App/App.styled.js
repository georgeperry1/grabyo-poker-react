import styled from 'styled-components';

export const Footer = styled.footer`
  border-top: 1px solid #eee;
  margin-top: 20px;
  padding: 20px 0;
`;
