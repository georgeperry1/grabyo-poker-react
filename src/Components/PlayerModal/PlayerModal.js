import React from 'react';
import PropTypes from 'prop-types';

import {
  StyledModal,
  customModalStyles,
  ModalBody,
  ModalHeading,
  ModalInput,
  ModalButton,
  ModalButtonContainer,
} from './PlayerModal.styled';

const PlayerModal = ({
  isOpen,
  handleModalClose,
  handleConfirmClick,
  handleInputChange,
  type,
}) => (
  <StyledModal isOpen={isOpen} style={customModalStyles}>
    <ModalBody>
      <ModalHeading>
        {type === 'add' ? 'Add a New Player' : 'Edit Player'}
      </ModalHeading>
      <ModalInput onChange={handleInputChange} />
      <ModalButtonContainer>
        <ModalButton onClick={handleConfirmClick}>
          {type === 'add' ? 'Add' : 'Save'}
        </ModalButton>
        <ModalButton onClick={handleModalClose}>Cancel</ModalButton>
      </ModalButtonContainer>
    </ModalBody>
  </StyledModal>
);

PlayerModal.propTypes = {
  handleConfirmClick: PropTypes.func,
  handleInputChange: PropTypes.func,
  handleModalClose: PropTypes.func,
  type: PropTypes.string,
  isOpen: PropTypes.bool,
};

PlayerModal.defaultProps = {
  handleConfirmClick: () => {},
  handleModalClose: () => {},
  handleInputChange: () => {},
  isOpen: false,
};

export default PlayerModal;
