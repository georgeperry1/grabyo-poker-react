import styled from 'styled-components';
import ReactModal from 'react-modal';

import { StyledButton } from '../Button/Button.styled';

export const StyledModal = styled(ReactModal)`
  background: #fff;
  border: 1px solid #ddd;
  border-radius: 3px;
  outline: none;
  padding: 20px;
  background: #282c34;
`;

export const customModalStyles = {
  content: {
    position: 'relative',
    overflow: 'auto',
    maxWidth: '30%',
    marginTop: '5%',
    marginBottom: '5%',
    left: '35%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
  },
  overlay: {
    margin: 0,
    position: 'fixed',
    zIndex: 11,
  },
};

export const ModalBody = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 10px;
`;

export const ModalHeading = styled.h2`
  color: #eee;
  font-weight: 700;
  font-size: 18px;
  margin-bottom: 20px;
`;

export const ModalInput = styled.input`
  height: 30px;
  width: 100%;
  border: 1px solid #ddd;
  border-radius: 3px;
  font-size: 16px;
  padding: 0px 5px;
`;

export const ModalButtonContainer = styled.div`
  display: flex;
  flex-direction: row;
  width: 50%;
  justify-content: space-around;
  margin-top: 20px;
`;

export const ModalButton = styled(StyledButton)`
  width: 200px;
  height: 30px;
`;
