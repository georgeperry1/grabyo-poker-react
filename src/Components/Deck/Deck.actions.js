import { SEND_HAND_TO_PLAYER, RESET_DECK } from './Deck.actionTypes';

export const sendHandToPlayer = hand => dispatch => {
  dispatch({
    type: SEND_HAND_TO_PLAYER,
    payload: hand,
  });
};

export const resetDeck = () => dispatch => {
  dispatch({
    type: RESET_DECK,
  });
};
