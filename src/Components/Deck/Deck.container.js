import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Deck from './Deck';
import { sendHandToPlayer } from './Deck.actions';
import { getDeck, fiveCardDeal } from '../../utils';

class DeckContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      board: [],
      deck: getDeck(),
      shuffling: false,
    };
  }

  static propTypes = {
    players: PropTypes.array,
    sendHandToPlayer: PropTypes.func,
  };

  static defaultProps = {
    sendHandToPlayer: () => {},
  };

  componentDidMount() {
    this.newRound();
  }

  newRound = () => {
    // Gets new shuffled deck after each round of poker
    const newDeck = getDeck();
    this.setState({ deck: newDeck });
  };

  dealHands = () => {
    const { deck } = this.state;
    const { players, sendHandToPlayer } = this.props;
    // Deals five cards to each player then sends via Redux
    players.map(player => { // eslint-disable-line
      const hand = fiveCardDeal(0, 5).map(e => deck.pop());
      sendHandToPlayer(hand);
    });
  };

  handleDealCardsClick = e => {
    // Every new 'Deal Cards' button click resets the deck
    this.newRound();
    this.dealHands();
  };

  handleShuffleAnimation = e => {
    // Shuffle deck animation is merely visual, 
    // shuffling happens when the 'Deal Cards' button is clicked
    this.setState({ shuffling: true });
  };

  handleEndShuffle = e => {
    this.setState({ shuffling: false });
  };

  render() {
    const { shuffling } = this.state;

    return (
      <Deck
        shuffling={shuffling}
        handleDealCardsClick={this.handleDealCardsClick}
        handleShuffleAnimation={this.handleShuffleAnimation}
        handleEndShuffle={this.handleEndShuffle}
        {...this.props}
      />
    );
  }
}

const mapStateToProps = ({ player }) => ({
  players: player.players,
});

const mapDispatchToProps = dispatch => ({
  sendHandToPlayer: hand => dispatch(sendHandToPlayer(hand)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeckContainer);
