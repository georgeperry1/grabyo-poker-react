import React from 'react';
import PropTypes from 'prop-types';

import {
  StyledDeck,
  DeckDisplay,
  DealButton,
  DeckCardOne,
  DeckCardTwo,
  DeckCardThree,
  DealButtonContainer,
} from './Deck.styled';

const Deck = ({
  shuffling,
  players,
  handleDealCardsClick,
  handleShuffleAnimation,
  handleEndShuffle,
}) => (
  <StyledDeck>
    <DeckDisplay>
      <DeckCardOne
        hidden
        onAnimationEnd={handleEndShuffle}
        className={shuffling ? 'shuffling' : ''}
      />
      <DeckCardTwo
        hidden
        onAnimationEnd={handleEndShuffle}
        className={shuffling ? 'shuffling' : ''}
      />
      <DeckCardThree hidden />
    </DeckDisplay>
    <DealButtonContainer>
      <DealButton icon="🔀" onClick={handleShuffleAnimation}>
        Shuffle Deck
      </DealButton>
      <DealButton
        icon="🃏"
        onClick={handleDealCardsClick}
        disabled={players.length === 0}
      >
        Deal Hands
      </DealButton>
    </DealButtonContainer>
  </StyledDeck>
);

Deck.propTypes = {
  shuffling: PropTypes.bool,
  players: PropTypes.array,
  handleDealCardsClick: PropTypes.func,
  handleShuffleAnimation: PropTypes.func,
  handleEndShuffle: PropTypes.func,
};

Deck.defaultProps = {
  shuffling: false,
  handleDealCardsClick: () => {},
  handleShuffleAnimation: () => {},
  handleEndShuffle: () => {},
};

export default Deck;
