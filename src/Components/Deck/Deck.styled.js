import styled, { keyframes } from 'styled-components';

import { HiddenCard } from '../Card/Card.styled';
import Card from '../Card';
import Button from '../Button';

export const StyledDeck = styled.div`
  display: flex;
  flex-direction: column;
`;

export const DeckDisplay = styled.section`
  height: 130px;

  ${HiddenCard} {
    width: 70px;
    height: 110px;
  }
`;

export const shuffleRight = keyframes`
    10% {
      -webkit-transform: translateX(8px) rotate(5deg);
      transform: translateX(8px) rotate(5deg);
    }
    20% {
      -webkit-transform: translateX(-8px) rotate(-5deg);
      transform: translateX(-8px) rotate(-5deg);
    }
	30% {
      -webkit-transform: translateX(8px) rotate(5deg);
      transform: translateX(8px) rotate(5deg);
    }
    40% {
      -webkit-transform: translateX(-8px) rotate(-5deg);
      transform: translateX(-8px) rotate(-5deg);
    }
	50% {
      -webkit-transform: translateX(8px) rotate(5deg);
      transform: translateX(8px) rotate(5deg);
    }
    60% {
      -webkit-transform: translateX(-8px) rotate(-5deg);
      transform: translateX(-8px) rotate(-5deg);
    }
	70% {
      -webkit-transform: translateX(8px) rotate(5deg);
      transform: translateX(8px) rotate(5deg);
    }
    80% {
      -webkit-transform: translateX(-8px) rotate(-5deg);
      transform: translateX(-8px) rotate(-5deg);
    }
	90% {
      -webkit-transform: translateX(8px) rotate(5deg);
      transform: translateX(8px) rotate(5deg);
    }
    100% {
      -webkit-transform: translateX(-8px) rotate(-5deg);
      transform: translateX(-8px) rotate(-5deg);
    }
`;

export const shuffleLeft = keyframes`
    10% {
      -webkit-transform: translateX(-4px) rotate(10deg);
      transform: translateX(-4px) rotate(10deg);
    }
    20% {
      -webkit-transform: translateX(4px) rotate(-10deg);
      transform: translateX(4px) rotate(-10deg);
    }
	30% {
      -webkit-transform: translateX(-4px) rotate(10deg);
      transform: translateX(-4px) rotate(10deg);
    }
    40% {
      -webkit-transform: translateX(4px) rotate(-10deg);
      transform: translateX(4px) rotate(-10deg);
    }
	50% {
      -webkit-transform: translateX(-4px) rotate(10deg);
      transform: translateX(-4px) rotate(10deg);
    }
    60% {
      -webkit-transform: translateX(4px) rotate(-10deg);
      transform: translateX(4px) rotate(-10deg);
    }
	70% {
      -webkit-transform: translateX(-4px) rotate(10deg);
      transform: translateX(-4px) rotate(10deg);
    }
    80% {
      -webkit-transform: translateX(4px) rotate(-10deg);
      transform: translateX(4px) rotate(-10deg);
    }
	90% {
      -webkit-transform: translateX(-4px) rotate(10deg);
      transform: translateX(-4px) rotate(10deg);
    }
    100% {
      -webkit-transform: translateX(4px) rotate(-10deg);
      transform: translateX(4px) rotate(-10deg);
    }
`;

export const DeckCardOne = styled(Card)`
  position: absolute;
  z-index: auto;

  &.shuffling {
    -webkit-animation-name: ${shuffleRight};
    animation-name: ${shuffleRight};
    animation-duration: 3s;
    animation-iteration-count: 1;
    transition: all 280ms ease-in-out;
  }
`;

export const DeckCardTwo = styled(Card)`
  position: absolute;
  z-index: auto;

  &.shuffling {
    -webkit-animation-name: ${shuffleLeft};
    animation-name: ${shuffleLeft};
    animation-duration: 3s;
    animation-iteration-count: 1;
    transition: all 280ms ease-in-out;
  }
`;

export const DeckCardThree = styled(Card)`
  position: absolute;
  z-index: 2;
`;

export const DealButtonContainer = styled.div`
  display: flex;
  flex-direction: row;
  width: 210px;
  justify-content: space-around;
  margin-top: 20px;
`;

export const DealButton = styled(Button)`
  width: 60px;
`;
