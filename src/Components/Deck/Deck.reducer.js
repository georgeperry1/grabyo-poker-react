import { SEND_HAND_TO_PLAYER, RESET_DECK } from './Deck.actionTypes';
import { REMOVE_PLAYER } from '../Player/Player.actionTypes';

const defaultState = {
  hands: [],
};

const deckReducer = (
  state = defaultState,
  action = {
    type: '',
    payload: '',
  }
) => {
  switch (action.type) {
    case SEND_HAND_TO_PLAYER:
      return {
        ...state,
        hands: [...state.hands, action.payload],
      };

    case RESET_DECK:
      return {
        ...state,
        hands: [],
      };

    case REMOVE_PLAYER:
      return {
        ...state,
        hands: [],
      };

    default:
      return state;
  }
};

export default deckReducer;
