import poker from 'poker-hands';

export const values = [
  '2',
  '3',
  '4',
  '5',
  '6',
  '7',
  '8',
  '9',
  'T',
  'J',
  'Q',
  'K',
  'A',
];
export const suits = ['D', 'H', 'S', 'C'];

export const getColourForSuit = suit =>
  suit === 'D' || suit === 'H' ? 'red' : 'black';

// Shuffles deck by randomising the index of all the cards
// then pushes the random index to a new deck
export function shuffleDeck(deck) {
  const newDeck = [];
  let n = deck.length;
  let i;

  while (n) {
    // Picks a card at random to insert into the new deck
    i = Math.floor(Math.random() * deck.length);

    // Checks if card has been pushed
    if (i in deck) {
      newDeck.push(deck[i]);
      delete deck[i];
      n--;
    }
  }

  return newDeck;
}

// Adds suits and values to make each card
// Shuffles using the above function
export const getDeck = () => {
  const deck = values
    .map(value => suits.map(suit => value + suit))
    .reduce((prev, curr) => prev.concat(curr));

  return shuffleDeck(deck);
};

// Tells the 'dealer' function to strong at 5 cards and not loop throught the entire deck
export const fiveCardDeal = (start, count) => {
  return Array.apply(0, Array(count)).map((element, index) => {
    return index + start;
  });
};

export const compareHands = hands => {
  // Loops through the hands and compares adjacent hands,
  // essentially a 'winner stays on' function 
  const winningHand = hands.reduce((winner, currentHand, index) => {
    // If the new hand wins, the currentHand is set as the winner
    const judge = poker.judgeWinner([currentHand, winner[0]]);
    if (judge === 0) winner = [currentHand];
    
    return winner;
  }, [hands[0]]); //First loop compares the first hand with itself as no winner is currently set

  return winningHand;
};
